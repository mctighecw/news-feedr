const express = require("express");
const morgan = require("morgan");

const app = express();
const port = process.env.PORT || 5000;

app.use(morgan("common"));

// directory for assets (css, js, etc.)
app.use(express.static(__dirname + "/"));

app.get("/*", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.listen(port, () => {
  console.log(`Node server is running on port ${port}`);
});

process.on("SIGINT", () => {
  console.info("Stopping Node server");
  process.exit(0);
});
