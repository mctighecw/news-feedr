FROM node:16.17-slim

WORKDIR /usr/src/app

COPY . .
RUN yarn install --silent

CMD ["node", "server.js"]
