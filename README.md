# README

This is a small project to practice AngularJS skills. It is a news article RSS feed site.

## App Information

App Name: news-feedr

Created: July 2016

Updated: April 2017; November 2018; September 2022

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/news-feedr)

Production: [Link](https://news-feedr.mctighecw.site)

## Run with Docker

```
$ docker build -t news-feedr .
$ docker run --rm -it -p 80:5000 news-feedr
```

## Notes

- AngularJS version 1.5.8

- A few different web tutorials were consulted and code models were followed.

- The Google Feed API was taken down in December 2016, so I have replaced it
  with this RSS to JSON converter [Link](https://rss2json.com).

- Originally deployed on Heroku

- Moved to personal server in September 2022

Any feedback or comments would be appreciated.

Last updated: 2025-01-03
